# stunning

Stunning is an alternative for stunnnel written all in Golang. It's a lib that help you to tunnel different type of network traffic.
So far `Tcp`, `Udp`, `Tls`, `Http` and `Https` tunnels are implemented and You can use `Udp Socket`, `Tcp Socket`, `Socks5` and `Tun Device` as your interface.

In future it's planned to add a terminal interface. 

There are test written for socks and tcp sockets and more tests would be added in future.

helps would be appreciated!